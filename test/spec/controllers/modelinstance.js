'use strict';

describe('Controller: ModelinstanceCtrl', function () {

  // load the controller's module
  beforeEach(module('mapVisualizationApp'));

  var ModelinstanceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ModelinstanceCtrl = $controller('ModelinstanceCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
