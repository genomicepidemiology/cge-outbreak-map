'use strict';

describe('Controller: AnalysisresultCtrl', function () {

  // load the controller's module
  beforeEach(module('mapVisualizationApp'));

  var AnalysisresultCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AnalysisresultCtrl = $controller('AnalysisresultCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
