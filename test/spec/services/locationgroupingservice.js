'use strict';

describe('Service: Locationgroupingservice', function () {

  // load the service's module
  beforeEach(module('mapVisualizationApp'));

  // instantiate service
  var Locationgroupingservice;
  beforeEach(inject(function (_Locationgroupingservice_) {
    Locationgroupingservice = _Locationgroupingservice_;
  }));

  it('should do something', function () {
    expect(!!Locationgroupingservice).toBe(true);
  });

});
