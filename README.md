CGE Tools: Map Visualization
===============

CGE Tool to visualize isolates on a map

The last working version can be found [here](https://cge.cbs.dtu.dk/tools/client/map/index.html)

Documentation
------
The following technologies have been used:

* [Leaflet](http://leafletjs.com/)
* [D3](http://d3js.org/)
* [Crossfilter](http://square.githubio/crossfilter/)
* [DC.js](http://dc-js.github.io/dc.js/)
* [AngularJS](https://angularjs.org/)

Usage
------

To test the map, [Node.js](http://nodejs.org/), [Grunt](http://gruntjs.com/)
and [Bower](http://bower.io/) should be installed.

You can follow [this tutorial](http://www.awmoore.com/2015/01/14/coding-in-windows-part-1/).

To install all dependencies run

```bash
make install
```

For testing and to install all browser dependencies:
```bash
grunt serve
```

To build without pushing the distribution to Bitbucket:
```bash
grunt build-locally
```
which generates a dist folder with a portable version of the map

Deployment
------
To create a distribution version of the app be sure to commit all your changes
before you run ```grunt build```. This command produces a folder called built and
push it to a new branch on the git repository called production-map-client.

To deploy the map visualization app use the following command:

```bash
git clone git@bitbucket.org:genomicepidemiology/cge-tools.git\
 -b production-map-client\
--single-branch map

```

in the client folder and use `get pull` to get new updates.

Development
------
If you want to contrinute, this is [the development model used](http://nvie.com/posts/a-successful-git-branching-model/) via this git [extension](https://github.com/nvie/gitflow)

Authors
------

* Jose Luis Bellod Cisneros
* Valentin Ibanez
